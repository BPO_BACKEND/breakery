(function () {
  'use strict';

  // Users directive used to force lowercase input
  angular
    .module('bakerys')
    .directive('fancybox', function($compile) {
  return {
    restrict: 'A',
    replace: false,
    link: function($scope, element, attrs) {

      $scope.open_fancybox = function() {

        var el = angular.element(element.html()),

        compiled = $compile(el);

        $.fancybox.open(el);

        compiled($scope);

      };
    }
  };
});
}());
